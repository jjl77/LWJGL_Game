package utils;

import java.math.*;

public class Vector 
{
	//Main class used to represent a vector in 3d space
	public class Vector3
	{
		public double x;
		public double y;
		public double z;
		public Vector3(double x, double y, double z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}
		/* Basic vector3 related functions */
		double GetX()
		{
			return this.x;
		}
		double GetY()
		{
			return this.y;
		}
		double GetZ()
		{
			return this.z;
		}
		
		/* Allows for setting variables */
		void SetX(double val)
		{
			this.x = val;
		}
		void SetY(double val)
		{
			this.y = val;
		}
		void SetZ(double val)
		{
			this.z = val;
		}
	}
}
